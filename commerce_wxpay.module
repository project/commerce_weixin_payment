<?php
/**
 * @file
 * Implements Weixin payment in Drupal Commerce.
 */

//TODO watchdog only work than the debug mode is open

/**
 * Implements hook_menu().
 */
function commerce_wxpay_menu() {
  // Add a menu callback for Weixin payment's notification on operation processing.
  $items['commerce_wxpay/notify'] = array(
    'page callback' => 'commerce_wxpay_process_notify',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['commerce_wxpay/qrcode'] = array(
    'page callback' => 'commerce_wxpay_qrcode_png',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['admin/commerce_wxpay/query/%commerce_payment_transaction'] = array(
    'page callback' => 'commerce_wxpay_query_remote_order',
    'page arguments' => array(3),
    'access callback' => 'commerce_payment_transaction_access',
    'access arguments' => array('view', 2),
  );

  $items['commerce_wxpay/payment/%commerce_order'] = array(
    'title' => 'Checkout Weixin Payment',
    'description' => 'The form page of weixin payment.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_wxpay_payment_form', 2),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_wxpay_commerce_payment_method_info() {
  $payment_methods = array();

  // Declare Weixin payment method to redirect to external site.
  $payment_methods['wxpay'] = array(
    'base' => 'commerce_wxpay',
    'title' => t('Weixin Payment'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}

/**
 * Implements hook_commerce_order_status_info().
 */
function commerce_wxpay_commerce_order_status_info() {
  $order_statuses = array();

  // Status used for trade types of cases.
  $order_statuses['wxpay_pay_success'] = array(
    'name' => 'wxpay_pay_success',
    'title' => t('Payment Success'),
    'state' => 'pending',
  );

  return $order_statuses;
}

/**
 * Payment method callback: settings form.
 */
function commerce_wxpay_settings_form($settings = NULL) {
  $form = array();

  $settings = $settings + array(
    'appid' => '',
    'mch_id' => '',
    'key' => '',
    'debug' => FALSE,
    'regenerate_order_number' => TRUE
  );

  $form['appid'] = array(
    '#type' => 'textfield',
    '#title' => t('APP ID'),
    '#description' => t("The id of the app associated with the payment method"),
    '#default_value' => $settings['appid'],
    '#required' => TRUE,
  );

  $form['mch_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#description' => t("The Merchant ID of the MP account on which payments should be credited."),
    '#default_value' => $settings['mch_id'],
    '#required' => TRUE,
  );

  $form['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant Key'),
    '#description' => t("The Secret Key of the MP account on which payments should be credited."),
    '#default_value' => $settings['key'],
    '#required' => TRUE,
  );

  $form['regenerate_order_number'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable order number regenerate'),
    '#description' => t('Whether to regenerate the order number with the custom number rule defined by this module'),
    '#default_value' => $settings['regenerate_order_number'],
  );

  $form['debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug mode <strong>(for development use only)</strong>'),
    '#description' => t('<strong>Override all transactions to a total of 0.01 CNY</strong> for testing the configuration and making sure that payments can be received on the correct account.<br/>This setting should only be used for development purposes.'),
    '#default_value' => $settings['debug'],
  );

  return $form;
}

/**
 * Init the session openid in checkout first page.
 */
function commerce_wxpay_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'commerce_checkout_form_checkout') {
    weixin_platform_session_openid();
  }
}

/**
 * Payment method callback: adds a message to the submission form.
 */
function commerce_wxpay_submit_form($payment_method, $pane_values, $checkout_pane, $order) {

  $form['wxpay_information'] = array(
    '#markup' => '<span class="commerce-alipay-info">' . t('(Continue with checkout to complete payment via Weixin.)') . '</span>',
  );
  $form['openid'] = array(
    '#type' => 'value',
    '#value' => weixin_platform_session_openid(),
  );

  return $form;
}

/**
 * Implements CALLBACK_commerce_payment_method_submit_form_validate()
 */
function commerce_wxpay_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  $prefix = implode('][', $form_parents) . '][';
  // Create the unified order

  $trade_type = 'NATIVE';
  if (weixin_platform_jsapi_is_enable()) {
    $trade_type = 'JSAPI';
  }
  // send request
  $response_data = commerce_wxpay_api_request($payment_method, $order, array('trade_type' => $trade_type, 'openid' => $pane_values['openid']));

  // handle the result.
  if ($response_data['return_code'] == 'SUCCESS') {
    watchdog('commerce_wxpay', 'unifiedorder created and response data: <pre>@response_data</pre>', array('@response_data' => $response->data));

    if ($response_data['result_code'] == 'SUCCESS') {
      $transactions = commerce_payment_transaction_load_multiple(array(), array(
                        'remote_id' => $response_data["prepay_id"],
                        'remote_status' => 'prepay',
                      ));
      if (!empty($transactions)) {
        return TRUE;
      }
      else {
        // Create a new payment transaction for the order.
        $transaction = commerce_wxpay_transaction_create($payment_method, $order);

        $transaction->remote_id = $response_data["prepay_id"];
        $transaction->remote_status = 'prepay';
        $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;

        if ($response_data['trade_type'] == 'NATIVE') {
          $transaction->data['code_url'] = $response_data["code_url"];
          $transaction->message = 'Prepay order for native trade type created';
        }
        else {
          $transaction->message = 'Prepay order for jsapi trade type created';
        }
        return commerce_payment_transaction_save($transaction);
      }
    }
    else {
      form_set_error($prefix . 'openid', t('Unified order create failed !message', array('!message' => $response_data['err_code_des'])));
      weixin_platform_error('Unified order create failed: !message', array('!message' => $response_data['err_code_des']));
      return FALSE;
    }
  }
  else {
    form_set_error($prefix . 'openid', t('Unified order create failed'));
    weixin_platform_error('Unified order create failed: !message', array('!message' => $response_data['return_msg']));
    return FALSE;
  }
}

/**
 * Helper function send wxpay api request
 */
function commerce_wxpay_api_request($payment_method, $order, $options = array()) {
  $xml = commerce_wxpay_build_unifiedorder_data($payment_method, $order, $options);

  $request_options = array(
    'method' => 'POST',
    'data' => $xml,
    'timeout' => 15,
    'headers' => array('Content-Type' => 'text/xml'),
  );

  $response = drupal_http_request('https://api.mch.weixin.qq.com/pay/unifiedorder', $request_options);
  return weixin_platform_xml_parse($response->data);
}

/**
 * Helper function for create new transaction of order
 */
function commerce_wxpay_transaction_create($payment_method, $order) {
  $transaction = commerce_payment_transaction_new($payment_method['method_id'], $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $order_total = $order_wrapper->commerce_order_total->value();

  $transaction->amount = $order_total['amount'];
  $transaction->currency_code = $order_total['currency_code'];

  return $transaction;
}

/**
 * Callback of redirect form
 */
function commerce_wxpay_redirect_form($form, &$form_state, $order, $payment_method) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed with payment'),
    '#submit' => array('commerce_wxpay_redirect'),
  );
  return $form;
}

/**
 * Redirect form submit callback
 */
function commerce_wxpay_redirect($form, &$form_state) {
  $order = $form_state['order'];
  drupal_goto('commerce_wxpay/payment/' . $order->order_id);
}

/**
 * Callback of weixin payment form
 */
function commerce_wxpay_payment_form($form, &$form_state, $order) {
  $form_state['order'] = $order;

  if (!empty($order->data['payment_method'])) {
    $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);

    $transactions = commerce_payment_transaction_load_multiple(array(), array(
                      'order_id' => $order->order_id,
                      'instance_id' => $payment_method['instance_id'],
                    ));

    if (empty($transactions)) {
      drupal_set_message(t('Prepay transaction do not be created'), 'error');
    }
    else {
      $transaction = reset($transactions);

      if (weixin_platform_jsapi_is_enable()) {
        // for jsapi trade type.
        $jsapi_settings = commerce_wxpay_jsapi_reqeust_params($payment_method['settings'], $transaction->remote_id);

        drupal_add_js(array('commerceWXPay' => $jsapi_settings), 'setting');
        drupal_add_js(drupal_get_path('module', 'commerce_wxpay') . '/commerce_wxpay.js');
      }
      else {
        if (!module_enable(array('nodejs_commerce_wxpay'))) {
          drupal_set_message('Weixin Payment native mode required the nodejs_commerce_wxpay module to be enabled');
        }

        $form['order_id'] = array(
          '#type' => 'value',
          '#value' => $transaction->order_id
        );
        // for native trade type.
        $png_url = url('commerce_wxpay/qrcode', array('query' => array('code_url' => urlencode($transaction->data['code_url']))));
        $form['code_url'] = array(
          '#type' => 'item',
          '#title' => t('Payment barcode'),
          '#markup' => theme('image__qrcode', array('path' => $png_url)),
          '#suffix' => '<div class="attach-message">' . t('After payment success, you need wait a moment, this page will be redirect automatic') . '</div>',
        );
      }

      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Complete payment'),
        '#prefix' => '<div style="display: none;">',
        '#suffix' => '</div>',
      );
    }
  }
  else {
    drupal_set_message(t('Current Order not allow checkout by Weixin payment.'), 'error');
  }

  return $form;
}

/**
 * Implements hook_libraries_info();
 */
function commerce_wxpay_libraries_info() {
  $libraries['phpqrcode'] = array(
    // Only used in administrative UI of Libraries API.
    'name' => 'PHP QrCode',
    'vendor url' => 'http://phpqrcode.sourceforge.net/',
    'download url' => 'http://sourceforge.net/apps/mediawiki/phpqrcode/index.php?title=Main_Page',
    // Specify arguments for the version callback. By default,
    // libraries_get_version() takes a named argument array:
    'version arguments' => array(
      'file' => 'VERSION',
      'pattern' => '/([0-9a-zA-Z\.-]+)/',
      'lines' => 2,
      'cols' => 20,
    ),
    'files' => array(
      'php' => array(
        'qrlib.php',
      ),
    ),
  );

  return $libraries;
}

/**
 * Payment form form submit callback
 */
function commerce_wxpay_payment_form_submit($form, &$form_state) {
  $order = $form_state['order'];

  if ($order->status != 'wxpay_pay_success') {
    $log = t('Customer order payment by weixin successed.');
    commerce_payment_redirect_pane_next_page($order, $log);
  }

  drupal_goto('checkout/' . $order->order_id . '/complete');
}

/**
 * Implements hook_entity_insert()
 */
function commerce_wxpay_entity_insert($entity, $entity_type) {
  if ($entity_type === 'commerce_order') {
    $payment_method = commerce_payment_method_instance_load('wxpay|commerce_payment_wxpay');
    if ($payment_method['settings']['regenerate_order_number']) {
      $entity->order_number = date('Ymd') . $entity->order_id;
    }
  }
}

/**
 * Build the data for unifiedorder api
 */
function commerce_wxpay_build_unifiedorder_data($payment_method, $order, $options = array()) {
  $options = $options + array('trade_type' => 'JSAPI');

  $payment_settings = $payment_method['settings'];
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $data = new stdClass;

  $data->appid = $payment_settings['appid'];
  $data->mch_id = $payment_settings['mch_id'];
  $data->device_info = "";
  $data->nonce_str = weixin_platform_noncestr();

  $body = commerce_wxpay_data_body($order);

  $data->body = mb_substr($body, 0, 10) . '...';
  $data->detail = $body;

  $data->attach = "";
  $data->out_trade_no = $order->order_number;

  $order_total = $order_wrapper->commerce_order_total->value();
  $data->fee_type = $order_total['currency_code'];

  $amount = $payment_settings['debug'] ? 1 : $order_total['amount'];
  $data->total_fee = $amount;

  $data->spbill_create_ip = ip_address();
  $data->time_start = date("YmdHis", $order->changed);
  $data->time_expire = date("YmdHis", $order->changed + 86400);
  $data->goods_tag = '';
  $data->notify_url = url('commerce_wxpay/notify', array('absolute' => TRUE));
  $data->trade_type = $options['trade_type'];

  if ($options['trade_type'] == 'JSAPI') {
    $data->openid = $options['openid'];
  }

  $data->sign = commerce_wxpay_sign($data, $payment_settings['key']);

  $request_xml = _commerce_wxpay_data_to_xml($data);
  watchdog('commerce_wxpay', 'Request XML <pre>@request_xml</pre>', array('@request_xml' => $request_xml));

  return $request_xml;
}

/**
 * Generate the request sign
 */
function commerce_wxpay_sign($data, $partner_key) {
  if (is_object($data)) {
    $data = get_object_vars($data);
  }
  ksort($data);

  $params = array();
  foreach ($data as $key => $value) {
    if (empty($value)) {
      continue;
    }
    $params[] = "$key=$value";
  }
  $params[] = "key=" . $partner_key;

  return strtoupper(md5(implode('&', $params)));
}

/**
 * Format data to xml
 */
function _commerce_wxpay_data_to_xml($data) {
  $data = get_object_vars($data);
  $doc = new DOMDocument('1.0', 'utf-8');
  $root = $doc->createElement('xml');
  $doc->appendChild($root);

  foreach ($data as $key => $value) {
    $element = $doc->createElement($key);
    $element->appendChild($doc->createTextNode($value));
    $root->appendChild($element);
  }

  return $doc->saveXML();
}

/**
 * Menu callback function to process Weixin payment's feedback notifications.
 */
function commerce_wxpay_process_notify() {
  $notify_data = file_get_contents('php://input');
  if (empty($notify_data)) {
    _commerce_wxpay_xml_output(array(
        'return_code' => '404',
        'return_msg' => 'Post data empty'
      ));
  }

  watchdog('commerce_wxpay', 'notify message: <pre>@notify_data</pre>', array('@notify_data' => $notify_data));
  $response_data = weixin_platform_xml_parse($notify_data);

  $order = null;
  $payment_method = commerce_payment_method_instance_load('wxpay|commerce_payment_wxpay');
  if ($payment_method['settings']['regenerate_order_number']) {
    $order = commerce_order_load_by_number($response_data['out_trade_no']);
  }
  else {
    $order = commerce_order_load($response_data['out_trade_no']);
  }

  if (empty($order)) {
    _commerce_wxpay_xml_output(array(
        'return_code' => '404',
        'return_msg' => 'Order not found'
      ));
  }

  // Validate the received notification from Alipay.
  if (commerce_wxpay_notify_validate($order, $payment_method, $response_data)) {
    commerce_wxpay_notify_submit($order, $payment_method, $response_data);
  }
}

/**
 * XML outpot helper
 */
function _commerce_wxpay_xml_output($data) {
  print _commerce_wxpay_data_to_xml((object)$data);
  drupal_page_footer();
  exit();
}

/**
 * Validation of weixin payment notifications.
 */
function commerce_wxpay_notify_validate($order, $payment_method, $response_data) {
  if ($response_data['return_code'] != 'SUCCESS') {
    weixin_platform_error('Notify post failed: !message', array('!message' => $response_data['return_msg']));
    return FALSE;
  }

  if ($response_data['result_code'] != 'SUCCESS') {
    weixin_platform_error('Notify post data failed: !message', array('!message' => $response_data['err_code_des']));
    return FALSE;
  }

  $remote_sign = $response_data['sign'];
  unset($response_data['sign']);

  $sign = commerce_wxpay_sign($response_data, $payment_method['settings']['key']);

  return $sign == $remote_sign;
}

/**
 * Submit and process a notification feedback from Alipay for a transaction.
 *
 * This function creates or updates payment transaction records for
 * corresponding orders depending on the status of the notifications received.
 */
function commerce_wxpay_notify_submit($order, $payment_method, $response_data) {
  $transactions = commerce_payment_transaction_load_multiple(array(), array('order_id' => $order->order_id));

  if (!empty($transactions)) {
    $transaction = reset($transactions);
  }
  else {
    // Create a new payment transaction for the order.
    $transaction = commerce_payment_transaction_new($payment_method['method_id'], $order->order_id);
    $transaction->instance_id = $payment_method['instance_id'];
    $transaction->amount = $response_data["total_fee"];
    $transaction->currency_code = $response_data["fee_type"];
    $transaction->remote_id = $response_data["transaction_id"];
    $transaction->message = 'Transaction completed by remote notify';
  }

  $transaction->remote_status = 'success';
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  commerce_payment_transaction_save($transaction);

  commerce_order_status_update($order, 'wxpay_pay_success');

  _commerce_wxpay_xml_output(array('return_code' => 'SUCCESS', 'return_msg' => 'OK'));
}

/**
 * Callback for body description.
 */
function commerce_wxpay_data_body($order) {
  if (empty($order)) {
    return '';
  }
  else {
    // Get a list of the items from the order to be paid.
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $line_items = $order_wrapper->commerce_line_items->value();
    foreach ($line_items as $line_item) {
      if ($line_item->type == 'product' && !empty($line_item->commerce_product)) {
        $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
        $body[] = $line_item_wrapper->commerce_product->title->value();
      }
    }
    return implode($body, ' | ');
  }
}

/**
 * Callback for remote order query page.
 */
function commerce_wxpay_query_remote_order($transaction) {
  $order = commerce_order_load($transaction->order_id);
  $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);
  $payment_settings = $payment_method['settings'];

  $response_data = _commerce_wxpay_query_remote_order_send_request($transaction->remote_id, $payment_settings);

  if ($response_data['return_code'] == 'SUCCESS' && $response_data['result_code'] == 'SUCCESS') {
    $rows = array(
      array(t('Bank type'), $response_data['bank_type']),
      array(t('Remote ID'), $response_data['transaction_id']),
      array(t('Order Id'), $response_data['out_trade_no']),
      array(t('Fee type'), $response_data['fee_type']),
      array(t('Message'), $response_data['attach']),
      array(t('Amount'), commerce_currency_format($response_data['total_fee'], $response_data['fee_type'])),
      array(t('Remote status'), check_plain($response_data['trade_state'])),
      array(t('Time end'), $response_data['time_end']),
    );
  }
  elseif ($response_data['result_code'] == 'FAIL') {
    $rows = array(
      array(t('Error Code'), $response_data['err_code']),
      array(t('Error Message'), $response_data['err_code_des'])
    );
  }
  else {
    $rows = array(
      array(t('Return Code'), $response_data['return_code']),
      array(t('Error Message'), $response_data['return_msg'])
    );
  }

  $content['remote_transaction_table'] = array(
    '#attached' => array(
      'css' => array(
        drupal_get_path('module', 'commerce_payment') . '/theme/commerce_payment.admin.css',
      ),
    ),
    '#markup' => theme('table', array('rows' => $rows, 'attributes' => array('class' => array('payment-transaction')))),
  );

  return $content;
}

/**
 * query remote order wrapper
 */
function _commerce_wxpay_query_remote_order_send_request($remote_id, $payment_settings) {
  $wx_settings = weixin_platform_settings();

  $data = new stdClass;

  $data->appid = $payment_settings['appid'];
  $data->mch_id = $payment_settings['mch_id'];
  $data->nonce_str = weixin_platform_noncestr();
  $data->transaction_id = $remote_id;
  $data->sign = commerce_wxpay_sign($data, $payment_settings['key']);

  $xml = _commerce_wxpay_data_to_xml($data);

  $options = array(
    'method' => 'POST',
    'data' => $xml,
    'timeout' => 15,
    'headers' => array('Content-Type' => 'text/xml'),
  );

  $response = drupal_http_request('https://api.mch.weixin.qq.com/pay/orderquery', $options);
  return weixin_platform_xml_parse($response->data);
}

/**
 * alert entity view of payment transaction
 */
function commerce_wxpay_commerce_payment_transaction_view_alter(&$build, $entity_type) {
  if ($build['#entity']->payment_method == 'wxpay') {
    $build['transaction_table']['#markup'] .= "<div class='inline-links'>" . l(t('View weixin transcation full information'), 'admin/commerce_wxpay/query/' . $build['#entity']->transaction_id) . "</div>";
  }
}

/**
 * QRcode png generate url
 */
function commerce_wxpay_qrcode_png() {
  if (libraries_load('phpqrcode')) {
    $url = urldecode($_GET["code_url"]);
    QRcode::png($url);
  }
}

/**
 * Constructs the pay request parameters
 */
function commerce_wxpay_jsapi_reqeust_params($payment_settings, $prepay_id) {
  $nonce_str = drupal_random_key(16);

  $jsapi_settings = array(
    'appId' => $payment_settings['appid'],
    'timeStamp' => REQUEST_TIME,
    'package' => 'prepay_id=' . $prepay_id,
    'nonceStr' => $nonce_str,
    'signType' => 'MD5',
  );

  $jsapi_settings['paySign'] = commerce_wxpay_sign($jsapi_settings, $payment_settings['key']);

  return $jsapi_settings;
}
